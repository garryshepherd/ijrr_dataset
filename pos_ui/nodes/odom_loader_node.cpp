#include <../include/pos_ui/pilot_ui.h>
#include <QApplication>
#include <QtCore/qglobal.h>
#include <ros/ros.h>
#include <../include/pos_ui/trajectory_dialog.h>

int main(int argc, char *argv[])
{

    ros::init(argc, argv, "odom_loader_node");
    ros::NodeHandlePtr node(new ros::NodeHandle("~"));
    QApplication a(argc, argv);
    TrajectoryDialog w;
    w.setWindowTitle(QString::fromStdString(ros::this_node::getName()));
    w.setup(node);
    w.show();


//    ConfigureSurvey w;
//    w.show();

    return a.exec();
}
