#ifndef DVL_STATUS_H
#define DVL_STATUS_H

#include <QWidget>
#include <ds_sensor_msgs/Dvl.h>
#include <algorithm>

#include "topic_widget.h"

namespace Ui {
class DVLStatus;
}

class DVLStatus : public TopicWidget
{
  Q_OBJECT

public:
  explicit DVLStatus(QWidget *parent = nullptr);
  ~DVLStatus();
  void setup(std::string topic, ros::NodeHandlePtr node);
  void callback(const ds_sensor_msgs::Dvl::ConstPtr& msg);

private:
  Ui::DVLStatus *ui;
};

#endif // DVL_STATUS_H
