#ifndef TRAJECTORY_DIALOG_H
#define TRAJECTORY_DIALOG_H

#include <QWidget>
#include <QFileDialog>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <boost/filesystem.hpp>
#include <map>
#include <unordered_set>
#include <memory>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PoseArray.h>
#include <ros/ros.h>

struct BagHandler{
  typedef std::shared_ptr<BagHandler> Ptr;
  rosbag::Bag bag;
  std::map<std::string , std::unordered_set<std::string>> bag_topics;
  rosbag::View::iterator view_itterator;
  std::shared_ptr<rosbag::View> view;
  void openBag(boost::filesystem::path filename);
};

namespace Ui {
class TrajectoryDialog;
}

class TrajectoryDialog : public QWidget
{
  Q_OBJECT

public:
  explicit TrajectoryDialog(QWidget *parent = nullptr);
  ~TrajectoryDialog();
  void setup(ros::NodeHandlePtr node);
  BagHandler::Ptr selectedHandler();
  void readBag();

private slots:
  void on_bag_btn_clicked();
  void on_selected_bag_activated(const QString &arg1);

  void on_add_odom_btn_clicked();

  void on_clear_btn_clicked();

private:
  Ui::TrajectoryDialog *ui;
  std::map<std::string,BagHandler::Ptr> bags_;
  geometry_msgs::PoseArray pose_array_;
  ros::NodeHandlePtr node_ptr_;
  ros::Publisher pub_;
};

#endif // TRAJECTORY_DIALOG_H
