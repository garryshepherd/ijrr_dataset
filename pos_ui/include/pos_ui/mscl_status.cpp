#include "mscl_status.h"
#include "ui_mscl_status.h"

MsclStatus::MsclStatus(QWidget *parent) :
    TopicWidget(parent),
    ui(new Ui::MsclStatus)
{
    ui->setupUi(this);
}

MsclStatus::~MsclStatus()
{
    delete ui;
}

void MsclStatus::setup(std::string topic, ros::NodeHandlePtr node){
    node_ptr_=node;
    sub_ = node_ptr_->subscribe<mscl_msgs::GpsCorrelationTimestampStamped>(topic,1,&MsclStatus::callback,this);
    ui->label->setText(QString::fromStdString(topic));
}

void MsclStatus::callback(const mscl_msgs::GpsCorrelationTimestampStamped::ConstPtr& msg){
    if( msg->gps_cor.timestamp_flags & mscl_msgs::GpsCorrelationTimestamp::TIMESTAMP_FLAG_PPS_GOOD ){
        ui->pps_status->setText(QString::fromStdString("GOOD"));
        ui->pps_status->setStyleSheet("color: green");
    }else {
        ui->pps_status->setText(QString::fromStdString("BAD"));
        ui->pps_status->setStyleSheet("color: red");
    }
    double dt = ros::Time::now().toSec() - msg->header.stamp.toSec();
    ui->delta_time->setNum(dt);
    if(dt<.05 && dt>0.0){
        ui->delta_time->setStyleSheet("color: green");
    }else if (dt>=.05 && dt<0.5) {
        ui->delta_time->setStyleSheet("color: yellow");
    }else{
        ui->delta_time->setStyleSheet("color: red");
    }
    resetTimer();
}
