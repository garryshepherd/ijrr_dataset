#ifndef MSCL_STATUS_H
#define MSCL_STATUS_H

#include <QWidget>
#include "topic_widget.h"
#include <mscl_msgs/GpsCorrelationTimestampStamped.h>

namespace Ui {
class MsclStatus;
}

class MsclStatus : public TopicWidget
{
    Q_OBJECT

public:
    explicit MsclStatus(QWidget *parent = nullptr);
    ~MsclStatus();
    void setup(std::string topic, ros::NodeHandlePtr node);
    void callback(const mscl_msgs::GpsCorrelationTimestampStamped::ConstPtr& msg);

private:
    Ui::MsclStatus *ui;

};

#endif // MCSL_STATUS_H
