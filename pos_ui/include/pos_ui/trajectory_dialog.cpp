#include "trajectory_dialog.h"
#include "ui_trajectory_dialog.h"

TrajectoryDialog::TrajectoryDialog(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::TrajectoryDialog)
{
  ui->setupUi(this);

}

TrajectoryDialog::~TrajectoryDialog()
{
  delete ui;
}

void TrajectoryDialog::setup(ros::NodeHandlePtr node){
  node_ptr_ = node;
  pub_ = node_ptr_->advertise<geometry_msgs::PoseArray>("trajectory",0,true);
}

void BagHandler::openBag(boost::filesystem::path filename){
  if(!boost::filesystem::exists(filename)){
    std::string error = filename.string()+" does not exist";
    throw std::runtime_error(error);
  }
  bag.open(filename.string());
  rosbag::View topic_view(bag);
  for (const rosbag::ConnectionInfo* info: topic_view.getConnections()) {
    bag_topics[info->datatype].insert(info->topic);
  }
  return;
}

void TrajectoryDialog::readBag(){
  std::vector<std::string> topics;
  topics.push_back(std::string(ui->topic->currentText().toStdString()));
  selectedHandler()->view.reset(new rosbag::View(selectedHandler()->bag,rosbag::TopicQuery(topics)));
  selectedHandler()->view_itterator=selectedHandler()->view->begin();

  ros::Time last;
  last.sec=0;
  last.nsec=0;
  ros::Duration dt;
  dt.fromSec(0.5);
  while(selectedHandler()->view_itterator!=selectedHandler()->view->end()){
    rosbag::MessageInstance const & m = *selectedHandler()->view_itterator;
    nav_msgs::Odometry::ConstPtr odom_msg = m.instantiate<nav_msgs::Odometry>();
    if(odom_msg){
      if( last + dt < odom_msg->header.stamp){
        pose_array_.header.frame_id = odom_msg->header.frame_id;
        pose_array_.poses.push_back(odom_msg->pose.pose);
        last = odom_msg->header.stamp;
      }
    }
    selectedHandler()->view_itterator++;
  }

  pose_array_.header.stamp = ros::Time::now();
  pub_.publish(pose_array_);
}

BagHandler::Ptr TrajectoryDialog::selectedHandler(){
  return bags_[ui->selected_bag->currentText().toStdString()];
}

void TrajectoryDialog::on_bag_btn_clicked()
{
  QString fileName = QFileDialog::getOpenFileName(this, tr("Specify A Bag File"),
                                                  "/home",
                                                  tr("Bag File (*.bag)"));
  this->setEnabled(false);
  boost::filesystem::path boost_file = fileName.toStdString();
  if(boost::filesystem::exists(boost_file)){
    BagHandler::Ptr newBag(new BagHandler);
    newBag->openBag(boost_file);
    bags_[boost_file.filename().string()]=newBag;
    ui->selected_bag->addItem(QString::fromStdString(boost_file.filename().string()));
  }
  this->setEnabled(true);
}

void TrajectoryDialog::on_selected_bag_activated(const QString &arg1)
{
  ui->topic->clear();
  for (auto topic : selectedHandler()->bag_topics["nav_msgs/Odometry"]) {
      ui->topic->addItem(QString::fromStdString(topic));
  }
}

void TrajectoryDialog::on_add_odom_btn_clicked()
{
  this->setEnabled(false);
  readBag();
  this->setEnabled(true);
}

void TrajectoryDialog::on_clear_btn_clicked()
{
  this->setEnabled(false);
  pose_array_.poses.clear();
  pose_array_.header.stamp = ros::Time::now();
  pub_.publish(pose_array_);
  this->setEnabled(true);
}
