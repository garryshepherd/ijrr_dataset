#include "configure_survey.h"
#include "ui_configuresurvey.h"

ConfigureSurvey::ConfigureSurvey(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::ConfigureSurvey)
{
  ui->setupUi(this);
  ui->buttonBox->button( QDialogButtonBox::Ok )->setEnabled( false );
  QWidget::setWindowTitle("Survey Configuration");
  refreshTopics();
  //ROS_INFO("refreshing topics");
}

ConfigureSurvey::~ConfigureSurvey()
{
  delete ui;
}

void ConfigureSurvey::refreshTopics(){
    ROS_INFO("refreshing topics");
    ros::master::V_TopicInfo master_topics;
    ros::master::getTopics(master_topics);

    for(ros::master::TopicInfo topic : master_topics){
        if(topic.datatype=="sensor_msgs/NavSatFix"){
            if(!gps_subs_[topic.name]){
                ROS_INFO("adding topic %s", topic.name.c_str());
                gps_subs_[topic.name].reset(new ros::Subscriber);
                *gps_subs_[topic.name] = _node.subscribe(topic.name,1,&ConfigureSurvey::gpsCallback,this);
                gps_msgs_[topic.name].reset(new sensor_msgs::NavSatFix);
                ui->gps_select->addItem(QString::fromStdString(topic.name));
            }

        }
    }
}

void ConfigureSurvey::gpsCallback(const ros::MessageEvent<sensor_msgs::NavSatFix const>& event){
    //ROS_INFO("got topic %s", event.getConnectionHeader().at("topic").c_str());
    *gps_msgs_[event.getConnectionHeader().at("topic")] = *event.getMessage();
}

sensor_msgs::NavSatFixPtr ConfigureSurvey::getCurrentFix(){
    return gps_msgs_[ui->gps_select->currentText().toStdString()];
}

void ConfigureSurvey::on_useCurrentLocationBtn_clicked()
{
    ui->originLatInput->setValue(getCurrentFix()->latitude);
    ui->originLonInput->setValue(getCurrentFix()->longitude);
    ui->originZInput->setValue(getCurrentFix()->altitude);
}

void ConfigureSurvey::on_browseBtn_clicked()
{
    enableInpuut(true);
    QString fileName = QFileDialog::getSaveFileName(this,
        tr("Specify Config Location"), "/home/untitled.surv", tr("survey configs (*.surv)"));
    ui->cfgFileInput->setText(fileName);
    //YAML::Node config = YAML::LoadFile(fileName.toStdString());
}

void ConfigureSurvey::on_buttonBox_accepted()
{
    sendParams();
    YAML::Node config;
    config["origin_lat"] = ui->originLatInput->value();
    config["origin_lon"] = ui->originLonInput->value();
    config["origin_z"]   = ui->originZInput->value();
    config["utm_zone"]   = ui->utmZoneInput->text().toStdString();
    config["timestamp"]  = ros::Time::now().toSec();

    std::string filename = ui->cfgFileInput->text().toStdString();
    std::ofstream fout(filename);

    fout << config;

    ui->cfgFileInput->setText("");
}

void ConfigureSurvey::on_loadCfgBtn_clicked()
{
    enableInpuut(true);
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Specify Config Location"), "/home/", tr("survey configs (*.surv)"));
    ui->cfgFileInput->setText(fileName);

    YAML::Node config = YAML::LoadFile(fileName.toStdString());
    ui->originLatInput->setValue(config["origin_lat"].as<double>());
    ui->originLonInput->setValue(config["origin_lon"].as<double>());
    ui->originZInput->setValue(config["origin_z"].as<double>());
    ui->utmZoneInput->setText( QString::fromUtf8(config["utm_zone"].as<std::string>().c_str()));
    enableInpuut(false);

}

void ConfigureSurvey::enableInpuut(bool x){
    ui->originLatInput->setEnabled(x);
    ui->originLonInput->setEnabled(x);
    ui->originZInput->setEnabled(x);
    ui->utmZoneInput->setEnabled(x);
    ui->useCurrentLocationBtn->setEnabled(x);
    ui->cfgFileInput->setEnabled(x);
}

void ConfigureSurvey::updateParams(){
    double origin_lat;
    _node.param<double>("/origin_lat",origin_lat,0);
    double origin_lon;
    _node.param<double>("/origin_lon",origin_lon,0);
    double origin_z;
    _node.param<double>("/origin_z",origin_z,0);
    std::string utm_zone;
    _node.param<std::string>("/utm_zone",utm_zone,"");


    ui->originLatInput->setValue(origin_lat);
    ui->originLonInput->setValue(origin_lon);
    ui->originZInput->setValue(origin_z);
    ui->utmZoneInput->setText(QString::fromUtf8(utm_zone.c_str()));

}

void ConfigureSurvey::sendParams(){
    ros::param::set("/origin_lat", ui->originLatInput->value());
    ros::param::set("/origin_lon",  ui->originLonInput->value());
    ros::param::set("/origin_z", ui->originZInput->value());
    ros::param::set("/utm_zone", ui->utmZoneInput->text().toStdString());
}

void ConfigureSurvey::on_cfgFileInput_textChanged(const QString &arg1)
{
    if(ui->cfgFileInput->text().trimmed().isEmpty()){
        ui->buttonBox->button( QDialogButtonBox::Ok )->setEnabled( false );
    }else {
        ui->buttonBox->button( QDialogButtonBox::Ok )->setEnabled( true );
    }
}

void ConfigureSurvey::on_buttonBox_rejected()
{
    QMessageBox Msgbox;
    Msgbox.setText("Operation Canceled.  No survey parameters set.");
    Msgbox.exec();
}

void ConfigureSurvey::on_refresh_btn_clicked()
{
    refreshTopics();
}
