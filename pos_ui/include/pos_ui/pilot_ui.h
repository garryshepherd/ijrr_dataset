#ifndef PILOTUI_H
#define PILOTUI_H

#include <QMainWindow>
#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/NavSatFix.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_types.h>
#include <pcl/PCLPointCloud2.h>
#include <pcl/conversions.h>
#include <pcl_ros/transforms.h>
#include <QTimer>
#include "configure_survey.h"
//#include <json/value.h>
#include "airmax_widget.h"
#include "battmon.h"
#include "ctd_widget.h"
#include "mscl_status.h"
#include "dvl_status.h"
#include "trajectory_dialog.h"

struct PilotUiParams{
    void fromServer(ros::NodeHandlePtr node_ptr){
        node_ptr->param<std::string>("topics/estimator_odom",   topics.estimator_odom,  "/odometry/filtered");
        node_ptr->param<std::string>("topics/multibeam_cloud",  topics.multibeam_cloud, "/wassp/detections");
        node_ptr->param<std::string>("topics/navsat_fix",       topics.navsat_fix,      "imu/raw");
        std::vector<std::string> default_stations{ };
        node_ptr->param<std::vector<std::string>>("topics/airmax_stations",topics.airmax_stations,default_stations);
        std::vector<std::string> default_batteries{ "/nav/sensors/navsat/batmon/bat" };
        node_ptr->param<std::vector<std::string>>("topics/batteries",topics.batteries,default_batteries);
        std::vector<std::string> default_ctds{ };
        node_ptr->param<std::vector<std::string>>("topics/ctds",topics.ctds,default_ctds);
        std::vector<std::string> default_mscl{ };
        node_ptr->param<std::vector<std::string>>("topics/mscl",topics.mscl,default_mscl);
        std::vector<std::string> default_dvl{ };
        node_ptr->param<std::vector<std::string>>("topics/dvl",topics.dvl,default_dvl);
    }
    struct{
        std::string estimator_odom;
        std::string multibeam_cloud;
        std::string navsat_fix;
        std::vector<std::string> airmax_stations;
        std::vector<std::string> batteries;
        std::vector<std::string> ctds;
        std::vector<std::string> mscl;
        std::vector<std::string> dvl;
    }topics;
};

namespace Ui {
class PilotUI;
}

class PilotUI : public QMainWindow
{
    Q_OBJECT

public:
    explicit PilotUI(QWidget *parent = nullptr);
    ~PilotUI();

    void setupTopics();
    //void setupBattery();

    void odomCallback(const nav_msgs::Odometry::ConstPtr& odomMsg);
    void scanCallback(const sensor_msgs::PointCloud2::ConstPtr& scan);
    void navsatCallback(const sensor_msgs::NavSatFixConstPtr& fix);

private slots:
    void on_cfgSurveyBtn_clicked();

private slots:
    void on_rangeValue_valueChanged(double arg1);
    void on_fullscreenBtn_clicked();
    void spin_ros();

private:
    Ui::PilotUI *ui;
    ros::NodeHandlePtr _node;
    ros::Subscriber _odomSub;
    ros::Subscriber _cloudSub;
    ros::Subscriber _navsatSub;
    //ros::AsyncSpinner spinner;
    QTimer *rosTimer;
    bool _fullscreen;
    ConfigureSurvey *_cfgSurveyDialog;
    PilotUiParams _params;
    std::map<std::string,AirmaxWidget*> airmax_map;
    std::map<std::string,BattMon*> battmon_map;
    std::map<std::string,CtdWidget*> ctds_map;
    std::map<std::string,MsclStatus*> mscl_map;
    std::map<std::string,DVLStatus*> dvl_map;
};

#endif // PILOTUI_H
