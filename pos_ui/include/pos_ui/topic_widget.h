#ifndef TOPICWIDGET_H
#define TOPICWIDGET_H

#include <QWidget>
#include <QTimer>
#include <ros/ros.h>

namespace Ui {
class TopicWidget;
}

class TopicWidget : public QWidget
{
    Q_OBJECT

public:
    explicit TopicWidget(QWidget *parent = nullptr);
    ~TopicWidget();
    virtual void setup(std::string topic, ros::NodeHandlePtr node);
    void setTimeout(int timeout_ms){timeout_=timeout_ms;}

protected slots:
    virtual void msgTimeout();

protected:
    void resetTimer();
    int timeout_;
    ros::NodeHandlePtr node_ptr_;
    ros::Subscriber sub_;
    QTimer *ui_timer_;
};

#endif // TOPICWIDGET_H
