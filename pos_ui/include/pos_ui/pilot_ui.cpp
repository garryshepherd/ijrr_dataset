#include "pilot_ui.h"
#include "ui_pilotui.h"

PilotUI::PilotUI(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::PilotUI)
{
    ui->setupUi(this);
    _node.reset(new ros::NodeHandle("~"));

    _params.fromServer(_node);

    _cfgSurveyDialog = new ConfigureSurvey(this);
    _odomSub = _node->subscribe<nav_msgs::Odometry> (_params.topics.estimator_odom, 1, &PilotUI::odomCallback, this);
    _cloudSub = _node->subscribe<sensor_msgs::PointCloud2> (_params.topics.multibeam_cloud, 1, &PilotUI::scanCallback, this);
    _navsatSub = _node->subscribe<sensor_msgs::NavSatFix> (_params.topics.navsat_fix,1, &PilotUI::navsatCallback, this);

    float range;
    _node->param<float>("/sona_range",range,20.0f);
    ui->rangeValue->setValue(range);

    float soundVel;
    _node->param<float>("/sound_velocity", soundVel , 1500.0f);

    rosTimer = new QTimer(this);
    connect(rosTimer, SIGNAL(timeout()), this, SLOT(spin_ros()));
    rosTimer->start(100);
    _fullscreen=false;

    setupTopics();
    //QPushButton* button = new QPushButton;
    //ui->HorizontalLayout->addWidget(button);
    //button->show();



}

PilotUI::~PilotUI()
{
    delete ui;
}

void PilotUI::setupTopics(){

    for (size_t i=0;i<_params.topics.dvl.size();i++) {
        dvl_map[_params.topics.dvl[i]] = new DVLStatus;
        dvl_map[_params.topics.dvl[i]]->setup(_params.topics.dvl[i],_node);
        ui->battery_container->addWidget(dvl_map[_params.topics.dvl[i]]);
    }
    for (size_t i=0;i<_params.topics.airmax_stations.size();i++) {
        airmax_map[_params.topics.airmax_stations[i]] = new AirmaxWidget;
        airmax_map[_params.topics.airmax_stations[i]]->setup(_params.topics.airmax_stations[i],_node);
        ui->airmax_container->addWidget(airmax_map[_params.topics.airmax_stations[i]]);
    }

    for (size_t i=0;i<_params.topics.batteries.size();i++) {
        battmon_map[_params.topics.batteries[i]] = new BattMon;
        battmon_map[_params.topics.batteries[i]]->setup(_params.topics.batteries[i],_node);
        ui->battery_container->addWidget(battmon_map[_params.topics.batteries[i]]);
    }

    for (size_t i=0;i<_params.topics.ctds.size();i++) {
        ctds_map[_params.topics.ctds[i]] = new CtdWidget;
        ctds_map[_params.topics.ctds[i]]->setup(_params.topics.ctds[i],_node);
        ui->battery_container->addWidget(ctds_map[_params.topics.ctds[i]]);
    }

    for (size_t i=0;i<_params.topics.mscl.size();i++) {
        mscl_map[_params.topics.mscl[i]] = new MsclStatus;
        mscl_map[_params.topics.mscl[i]]->setup(_params.topics.mscl[i],_node);
        ui->battery_container->addWidget(mscl_map[_params.topics.mscl[i]]);
    }
}

void PilotUI::spin_ros(){
  if(ros::ok()){
    ros::spinOnce();
  }else{
    this->close();
  }
}

void PilotUI::odomCallback(const nav_msgs::Odometry::ConstPtr &odomMsg){
    //ROS_INFO("callback");
    double speed = odomMsg->twist.twist.linear.x * odomMsg->twist.twist.linear.x + odomMsg->twist.twist.linear.y * odomMsg->twist.twist.linear.y;
    speed = sqrt(speed);
    speed = speed * 1.94384; //convert to kts
    QString speedLbl = QString::number(speed , 'f', 2);
    ui->speedValue->setText(speedLbl);
    return;
}

void PilotUI::scanCallback(const sensor_msgs::PointCloud2::ConstPtr &scan){
    if((scan->width*scan->height)>0){
        pcl::PointCloud<pcl::PointXYZ> pcl_cloud;
        pcl::fromROSMsg (*scan, pcl_cloud);
        QString depthTxt = QString::number(pcl_cloud(pcl_cloud.size()/2,0).z,'f', 2);
        ui->cbdValue->setText(depthTxt);
    }
}

void PilotUI::navsatCallback(const sensor_msgs::NavSatFixConstPtr &fix){
    double uncertainty = sqrt(fix->position_covariance[0] + fix->position_covariance[4]);
    ui->xUncertaintyValue->setNum( uncertainty );
    double vertUncertainty = sqrt(fix->position_covariance[8]);
    ui->verticalUncertaintyValue->setNum(vertUncertainty);
    _cfgSurveyDialog->currentFix = *fix;
}

void PilotUI::on_rangeValue_valueChanged(double arg1)
{
    _node->setParam("/sona_range", float(arg1));
}

void PilotUI::on_fullscreenBtn_clicked()
{
    isFullScreen() ? showNormal() : showFullScreen();
}


void PilotUI::on_cfgSurveyBtn_clicked()
{
    _cfgSurveyDialog->updateParams();
    _cfgSurveyDialog->show();
}
