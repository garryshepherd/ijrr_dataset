#include "ctd_widget.h"
#include "ui_ctd_widget.h"

CtdWidget::CtdWidget(QWidget *parent) :
    TopicWidget(parent),
    ui(new Ui::CtdWidget)
{
    ui->setupUi(this);
}

void CtdWidget::setup(std::string topic, ros::NodeHandlePtr node){
    node_ptr_=node;
    sub_ = node_ptr_->subscribe<ds_sensor_msgs::Ctd>(topic,1,&CtdWidget::ctdCallback,this);
    ui->label->setText(QString::fromStdString(topic));
}

void CtdWidget::ctdCallback(const ds_sensor_msgs::Ctd::ConstPtr &msg){
    ui->temp_lbl->setNum(msg->temperature);
    ui->sound_vel_lbl->setNum(msg->sound_speed);
    ui->salinty_lbl->setNum(msg->salinity);
    resetTimer();

}

CtdWidget::~CtdWidget()
{
    delete ui;
}
