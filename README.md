# README #
This repository is to accompany our publication in IJRR.   It offeres tools to process the datasets described in that paper. 

for more details visit [our website](https://seaward.science/data/pos/)


### How do I get set up? ###

* Install ros and setup your workspace: [details](http://wiki.ros.org/ROS/Installation)
* clone this package to your workspace src directory
* run catkin_make

### Basic Usage ###
There are a few launch files that will be useful for previewing the data

#### Visualize Bag ####
this tool will run our real time visualization GUI and replay a bag.   This is probably the best place to start to check that everything is working.   Be patient.   It will take a minute to load the bag files before it starts playing


```roslaunch pos visualize_bag.launch filename:=<path to bag file>  rate:=<optional how fast you want to replay>```
	
#### Replay Bag (raw sensor data) ####
This tool will replay the raw sensor data from the bag file.   Useful if you want to try your own nav solution.

```roslaunch pos roslaunch pos visualize_bag.launch filename:=<path to bag file>  rate:=<optional how fast you want to replay>```
	
#### Rerun Navigation Solution ####
You can re-compute the navigation solution using the `renav.launch` file.   It takes the following arguments:

* infile:   the path to the bag you want to re-process
* outfile:   the path to the bag you want to save with the new navigation solution
* surv:   The surv file you want to use.   This file specifies the local origin and UTM zone.   Each suvey dataset includes one for you to use
* rate (default 1):   how fast you want to replay the bag

You can also tweak the EKF settings by editing the `/pos/config/utm_ekf.yaml` config file and the `/pos/launch/utm_localization.launch` file launch file.   

