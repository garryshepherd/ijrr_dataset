#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 30 11:02:14 2020

@author: kris
"""

import rosbag


print("opening...")
input_bag = rosbag.Bag('/data/pos_datasets/reprocessed/wiggles_bank/wiggles_bank_renav_2020-11-27-17-07-37.bag')
#output_bag = rosbag.Bag('/data/pos_datasets/reprocessed/wiggles_bank/wiggles_bank_renav_cleaned.bag', 'w')

all_topics = input_bag.get_type_and_topic_info()[1].keys()


if ('/clock' in all_topics):
  all_topics.remove('/clock'),

print("processing...")

total = input_bag.get_message_count()
current = 0

for topic, msg, t in input_bag.read_messages( raw=True):
    if(current % 10000 == 0):
      print("processing: %i/%i,  %.2f percent complete" % (current,total,float(current)/float(total)*100.0))
#    output_bag.write(topic, msg)
    current=current+1

print("done...")
input_bag.close()
#output_bag.close()
